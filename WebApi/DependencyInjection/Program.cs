﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace DependencyInjection
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Test();

            Console.WriteLine();
            Console.WriteLine();

            DependencyTest();

            Console.WriteLine();
            Console.WriteLine();

            DependencyTransientTest();

            Console.WriteLine();
            Console.WriteLine();

            TransientTest();

            Console.WriteLine();
            Console.WriteLine();

            ScopedTest();

            Console.WriteLine();
            Console.WriteLine();

            DependencyScopedTest();
        }

        public static void Test()
        {
            var serviceA = new ServiceA();
            var serviceB = new ServiceB(serviceA);
            var serviceC = new ServiceC(serviceB);
            var serviceD = new ServiceD(serviceA);
        }

        public static void DependencyTest()
        {
            var services = new ServiceCollection();

            services
                .AddSingleton<ServiceA>()
                .AddSingleton<ServiceB>()
                .AddSingleton<ServiceC>()
                .AddSingleton<ServiceD>();

            var provider = services.BuildServiceProvider();

            var serviceA = provider.GetService<ServiceA>();
            var serviceB = provider.GetService<ServiceB>();
            var serviceC = provider.GetService<ServiceC>();
            var serviceD = provider.GetService<ServiceD>();
        }

        public static void DependencyTransientTest()
        {
            var services = new ServiceCollection();

            services
                .AddTransient<ServiceA>()
                .AddTransient<ServiceB>()
                .AddTransient<ServiceC>()
                .AddTransient<ServiceD>();

            var provider = services.BuildServiceProvider();

            var serviceA = provider.GetService<ServiceA>();
            var serviceB = provider.GetService<ServiceB>();
            var serviceC = provider.GetService<ServiceC>();
            var serviceD = provider.GetService<ServiceD>();
        }

        public static void TransientTest()
        {
            var serviceA = new ServiceA();
            var serviceB = new ServiceB(new ServiceA());
            var serviceC = new ServiceC(new ServiceB(new ServiceA()));
            var serviceD = new ServiceD(new ServiceA());
        }

        public static void ScopedTest()
        {
            Console.WriteLine("Scope 1:");
            var serviceA = new ServiceA();
            var serviceB = new ServiceB(serviceA);
            var serviceC = new ServiceC(new ServiceB(serviceA));
            var serviceD = new ServiceD(serviceA);

            // do work

            Console.WriteLine("Scope 2:");
            serviceA = new ServiceA();
            serviceB = new ServiceB(serviceA);
            serviceC = new ServiceC(new ServiceB(serviceA));
            serviceD = new ServiceD(serviceA);

            // do work
        }

        public static void DependencyScopedTest()
        {
            var services = new ServiceCollection();

            services
                .AddScoped<ServiceA>()
                .AddTransient<ServiceB>()
                .AddTransient<ServiceC>()
                .AddTransient<ServiceD>();

            var provider = services.BuildServiceProvider();

            Console.WriteLine("Scope 1:");
            using (var scope = provider.CreateScope())
            {
                var serviceA = scope.ServiceProvider.GetService<ServiceA>();
                var serviceB = scope.ServiceProvider.GetService<ServiceB>();
                var serviceC = scope.ServiceProvider.GetService<ServiceC>();
                var serviceD = scope.ServiceProvider.GetService<ServiceD>();

                // do work
            }

            Console.WriteLine("Scope 2:");
            using (var scope = provider.CreateScope())
            {
                var serviceA = scope.ServiceProvider.GetService<ServiceA>();
                var serviceB = scope.ServiceProvider.GetService<ServiceB>();
                var serviceC = scope.ServiceProvider.GetService<ServiceC>();
                var serviceD = scope.ServiceProvider.GetService<ServiceD>();

                // do work
            }
        }
    }

    public class ServiceA
    {
        public ServiceA()
        {
            Console.WriteLine("ServiceA created");
        }
    }

    public class ServiceB
    {
        public ServiceB(ServiceA serviceA)
        {
            Console.WriteLine("ServiceB created");
        }
    }

    public class ServiceC
    {
        public ServiceC(ServiceB serviceB)
        {
            Console.WriteLine("ServiceC created");
        }
    }

    public class ServiceD
    {
        public ServiceD(ServiceA serviceA)
        {
            Console.WriteLine("ServiceD created");
        }
    }
}
