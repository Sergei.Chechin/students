﻿using System;
using System.Linq;
using Microsoft.Extensions.Configuration;

namespace Configuration
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("MainConfig.json")
                .AddJsonFile("SecondConfig.json")
                .Build();

            foreach (var item in configuration.GetChildren())
            {
                Print(item);
            }
        }

        private static void Print(IConfigurationSection section)
        {
            var childs = section.GetChildren();

            if (!childs.Any())
            {
                Console.WriteLine("{0} -> {1}", section.Path, section.Value);
            }

            foreach (var item in childs)
            {
                Print(item);
            }
        }
    }
}
