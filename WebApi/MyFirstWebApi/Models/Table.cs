﻿using MyFirstWebApi.DataAccess;

namespace MyFirstWebApi.Models
{
    public class Table : IEntity
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public bool IsOccupied { get; set; }
    }
}
