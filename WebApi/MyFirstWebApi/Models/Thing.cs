﻿using MyFirstWebApi.DataAccess;

namespace MyFirstWebApi.Models
{
    public class Thing : IEntity
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public int Weight { get; set; }

        public long TableId { get; set; }

        public bool IsBroken { get; set; }
    }
}
