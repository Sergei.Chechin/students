﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace MyFirstWebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NotesController : ControllerBase
    {
        private static readonly Dictionary<Guid, string> _notes = new();

        [HttpGet("{id:guid}")]
        public string Get([FromRoute] Guid id)
        {
            return _notes[id];
        }

        [HttpGet]
        public IDictionary<Guid, string> GetAll()
        {
            return _notes;
        }

        [HttpGet("LastTwo")]
        public IEnumerable<string> GetLastTwo()
        {
            return _notes.Values
                .Skip(Math.Max(_notes.Count - 2, 0));
        }

        [HttpPost("{id:guid}/[action]")]
        public void Publish([FromBody] Guid id)
        {
        }

        [HttpPost]
        public Guid Create([FromBody] string note)
        {
            var id = Guid.NewGuid();
            _notes[id] = note;

            return id;
        }
    }
}
