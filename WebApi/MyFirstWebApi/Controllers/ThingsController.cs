﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MyFirstWebApi.DataAccess;
using MyFirstWebApi.Models;

namespace MyFirstWebApi.Controllers
{
    [Route("[controller]")]
    public class ThingsController : ControllerBase
    {
        private readonly IRepository<Thing> _repository;

        public ThingsController(IRepository<Thing> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IEnumerable<Thing> GetList()
        {
            return _repository.GetAll();
        }

        [HttpGet("{id:long}")]
        public IActionResult Get([FromRoute] long id)
        {
            var result = _repository.Get(id);
            return result == null ? NotFound() : Ok(result);
        }

        [HttpPost]
        public void Create([FromBody] Thing table)
        {
            _repository.Insert(table);
        }

        [HttpPut("{id:long}")]
        public IActionResult Update([FromRoute] long id, [FromBody] Thing table)
        {
            var result = _repository.Get(id);

            if (result == null)
            {
                return NotFound();
            }

            table.Id = id;

            _repository.Update(table);

            return Ok();
        }

        [HttpDelete("{id:long}")]
        public void Delete([FromRoute] long id)
        {
            _repository.Delete(id);
        }
    }
}
