﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace MyFirstWebApi.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ApiErrorController : ControllerBase
    {
        [HttpGet]
        public void FireInternal()
        {
            throw new Exception("This is internal test exception");
        }

        [HttpGet]
        public void FireArgument()
        {
            throw new ArgumentException("This is argument test exception");
        }

        [HttpGet]
        public void FileNotImplement()
        {
            throw new NotImplementedException();
        }
    }
}
