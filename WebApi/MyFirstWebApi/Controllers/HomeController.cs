﻿using System;
using System.Net;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace MyFirstWebApi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HomeController : ControllerBase
    {

        [Route("/Error")]
        public IActionResult HandleError()
        {
            var exceptionHandler = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            var ex = exceptionHandler.Error;
            string title, detail;
            int code;

            switch (ex)
            {
                case ArgumentException ae:
                    title = "Request error!";
                    detail = ae.Message;
                    code = (int)HttpStatusCode.BadRequest;
                    break;
                case NotImplementedException nimpl:
                    title = "This action not implement!";
                    detail = null;
                    code = (int)HttpStatusCode.NotImplemented;
                    break;
                default:
                    title = "Internal server error!";
                    detail = ex.ToString();
                    code = (int)HttpStatusCode.InternalServerError;
                    break;
            }

            return Problem(title: title, detail: detail, statusCode: code);
        }
    }
}
