﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MyFirstWebApi.DataAccess;
using MyFirstWebApi.Models;

namespace MyFirstWebApi.Controllers
{
    [Route("[controller]")]
    public class TablesController : ControllerBase
    {
        private readonly IRepository<Table> _repository;
        private readonly IRepository<Thing> _thingRepository;

        public TablesController(IRepository<Table> repository, IRepository<Thing> thingRepository)
        {
            _repository = repository;
            _thingRepository = thingRepository;
        }

        [HttpGet] //- GET Tables
        public IEnumerable<Table> GetAll()
        {
            return _repository.GetAll();
        }

        [HttpGet("{id:long}")] //- GET Tables/{id}
        public IActionResult Get([FromRoute] long id)
        {
            var result = _repository.Get(id);
            return result == null ? NotFound() : Ok(result);
        }

        [HttpPost] //- POST Tables \n { ... }
        public void Create([FromBody] Table table)
        {
            _repository.Insert(table);
        }

        [HttpPut("{id:long}")] //- PUT Tables/{id} \n { ... }
        public IActionResult Update([FromRoute] long id, [FromBody] Table table)
        {
            var result = _repository.Get(id);

            if (result == null)
            {
                return NotFound();
            }

            table.Id = id;

            _repository.Update(table);

            return Ok();
        }

        [HttpDelete("{id:long}")] //- DELETE Tables/{id}
        public void Delete([FromRoute] long id)
        {
            _repository.Delete(id);
        }

        [HttpPatch("{id:long}/[action]")] //- PATCH Tables/{id}/Occupied
        public void Occupied(long id)
        {
            var table = _repository.Get(id);

            table.IsOccupied = true;

            _repository.Update(table);
        }

        [HttpPatch("{id:long}/[action]")] //- PATCH Tables/{id}/Vacate
        public void Vacate(long id)
        {
            var table = _repository.Get(id);

            table.IsOccupied = false;

            _repository.Update(table);
        }

        [HttpGet("{id:long}/Things")]
        public IEnumerable<Thing> GetThings(long id)
        {
            return _thingRepository.GetAll().Where(x => x.TableId == id);
        }
    }
}
