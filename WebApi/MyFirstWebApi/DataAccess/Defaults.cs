﻿using System.Collections.Generic;
using MyFirstWebApi.Models;

namespace MyFirstWebApi.DataAccess
{
    internal static class Defaults
    {
        public static Dictionary<long, Table> Tabels = new()
        {
            { 1, new() { Id = 1, Name = "Office table #1", Type = "DeveloperTable" } },
            { 2, new() { Id = 2, Name = "Office table #2", Type = "DeveloperTable" } },
            { 3, new() { Id = 3, Name = "Office table #3", Type = "DeveloperTable" } },
            { 4, new() { Id = 4, Name = "Boss table", Type = "DeveloperTable" } },
            { 5, new() { Id = 5, Name = "Kitchen table", Type = "KitchenTable" } },
        };

        public static Dictionary<long, Thing> Things = new()
        {
            { 1, new() { Id = 1, IsBroken = false, Name = "HP Probook", TableId = 1, Weight = 1500 } },
            { 2, new() { Id = 2, IsBroken = false, Name = "Mouse", TableId = 1, Weight = 100 } },
            { 3, new() { Id = 3, IsBroken = false, Name = "Keyboard", TableId = 1, Weight = 300 } },
            { 4, new() { Id = 4, IsBroken = false, Name = "Headphone", TableId = 1, Weight = 250 } },

            { 5, new() { Id = 5, IsBroken = false, Name = "HP Probook", TableId = 2, Weight = 1500 } },
            { 6, new() { Id = 6, IsBroken = false, Name = "Mouse", TableId = 2, Weight = 100 } },
            { 7, new() { Id = 7, IsBroken = false, Name = "Keyboard", TableId = 2, Weight = 300 } },
            { 8, new() { Id = 8, IsBroken = false, Name = "Headphone", TableId = 2, Weight = 250 } },

            { 9, new() { Id = 9, IsBroken = true, Name = "HP Probook", TableId = 3, Weight = 1500 } },
            { 10, new() { Id = 10, IsBroken = false, Name = "Mouse", TableId = 3, Weight = 100 } },
            { 11, new() { Id = 11, IsBroken = false, Name = "Keyboard", TableId = 3, Weight = 300 } },
            { 12, new() { Id = 12, IsBroken = false, Name = "Headphone", TableId = 3, Weight = 250 } },

            { 13, new() { Id = 13, IsBroken = false, Name = "Macbook Pro", TableId = 4, Weight = 1500 } },
            { 14, new() { Id = 14, IsBroken = false, Name = "Mouse", TableId = 4, Weight = 100 } },
            { 15, new() { Id = 15, IsBroken = false, Name = "Keyboard", TableId = 4, Weight = 300 } },
            { 16, new() { Id = 16, IsBroken = false, Name = "Parker", TableId = 4, Weight = 50 } },
        };
    }
}
