﻿using System.Collections.Generic;

namespace MyFirstWebApi.DataAccess
{
    public interface IRepository<T>
        where T : class, IEntity
    {
        T Get(long id);
        IEnumerable<T> GetAll();
        void Insert(T entity);
        void Update(T entity);
        void Delete(long id);
    }

    public interface IEntity
    {
        long Id { get; set; }
    }
}
