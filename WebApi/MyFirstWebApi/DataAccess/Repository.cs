﻿using System.Collections.Generic;
using System.Linq;

namespace MyFirstWebApi.DataAccess
{
    public class Repository<T> : IRepository<T>
        where T : class, IEntity
    {
        private readonly Dictionary<long, T> _data;
        private long _sequence;

        public Repository(Dictionary<long, T> data)
        {
            _data = data;
            _sequence = data.Keys.Max();
        }

        public void Delete(long id)
        {
            _data.Remove(id);
        }

        public T Get(long id)
        {
            return _data.GetValueOrDefault(id);
        }

        public IEnumerable<T> GetAll()
        {
            return _data.Values;
        }

        public void Insert(T entity)
        {
            entity.Id = ++_sequence;
            _data[_sequence] = entity;
        }

        public void Update(T entity)
        {
            _data[entity.Id] = entity;
        }
    }
}
